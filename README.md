# HiNAT

HiNAT 内网穿透工具运用 FRP 功能，使用图形化设置界面，通过端口转发技术，将本地端口转发至服务端的目标端口，提供内网穿透服务，实现内网服务穿透至公网，支持的通信类型包括 TCP、UDP 与 HTTP，并支持常见物联网通信协定的端口转发，例如 WebSocket、MQTT、RestAPI、Modbus TCP ..等通信协定。

>**请加入 QQ 群(群号:309174897)**
> 
> <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=haIxTvkUQkjCE6pNy0dH6YTuuEFYSK5_&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="物联网、智联网、ERP、" title="物联网、智联网、ERP、"></a>
> 
> **请关注微信公众号 IsoFace 爱招飞**
> 
> ![wechat_isoface.png](https://s2.loli.net/2022/10/09/WIVGP5C1E4jqbJY.png)

HiNAT 包括服务端、控制端的内网穿透工具，提供远程控制和内网穿透服务的解决方案。HiNAT 控制端基于 FRP 内网穿透，提供快速流畅的远程控制服务。内网穿透工具为服务端，通过图形化界面设置，可提供高性能、高效的反向代理服务。

![](images/20230403155529.png)

特色：

1. 运用 FRP 内网穿透技术，远程控制带宽消耗低，网络延迟低，具有更好的远程控制效果。
2. 提供内网穿透服务程序，用户可自行部署运行测试，不依赖于公有云服务。
3. 提供图形化界面的内网穿透配置方式，避免设置档编写导致的内网穿透服务运行错误，直观简单。
4. 提供网页控制台，可对端口转发、内网穿透、反向代理服务状态进行监控与管理。

运用场景：

1. 客户即时远程遥控，进行技术支持服务。
2. 内网部署服务提供公网访问。
3. 部署串口服务，实现端口转发。
4. 支持反向代理服务，与 SSL 协定流量的转发。
5. 物联网设备的远程存取服务。 例如：NodRed、MySQL、Web，无需固定 IP 地址，都可在内网提供各种网络服务。